import React, { Component } from 'react';
import { Text, TouchableOpacity, Image, View, StyleSheet, ScrollView } from 'react-native';

class DetailsScreen extends Component {
  newsItem = {};
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.newsItem = props.route.params.data;
  }
  render() {
    return (
      <View style={{ backgroundColor: '#fff', flex: 1, paddingTop: 40 }}>
        <Image style={{
          flex: 1,
          width: '100%',
          height: '100%',
          resizeMode: 'contain',
        }}
          source={{ uri: this.newsItem.urlToImage }}
        />
        <ScrollView>
          <View style={{ display: "flex", flexDirection: 'row', paddingRight: 20, paddingLeft: 20, paddingTop: 20 }}>
            <View style={{ height: 40, width: 40, backgroundColor: "#d7d7d7", borderRadius: 100, marginRight: 10, marginTop: 2 }}></View>
            <View style={{ display: "flex", flexDirection: 'column' }}>
              <Text style={{ color: '#484f59', marginBottom: 5 }}>
                {this.newsItem.author}
              </Text>
              <Text style={{ color: '#484f59' }}>
                {getParsedDate(this.newsItem.publishedAt)}
              </Text>
            </View>
          </View>
          <View style={{ padding: 20 }}>
            <Text style={styles.title}>
              {this.newsItem.title}
            </Text>
            <Text style={{ fontSize: 14, marginBottom: 20 }}>
              {this.newsItem.description}
            </Text>

            <TouchableOpacity
              style={styles.loginScreenButton}
              onPress={() => this.props.navigation.navigate('WebViewScreen', { data: this.newsItem.url })}
              underlayColor='#fff'>
              <Text style={styles.loginText}>Read full article</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 16,
    fontWeight: '500',
    marginBottom: 8
  },
  image: {

    width: '100%',
    height: 200,
    marginBottom: 8
  },
  loginScreenButton: {
    marginRight: 40,
    marginLeft: 40,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#ec2227',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText: {
    color: '#fff',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10
  }
});

function getParsedDate(strDate: any) {
  let strSplitDate = String(strDate).split(' ');
  let date = new Date(strSplitDate[0]);
  // alert(date);
  let dd = date.getDate();
  let mm = date.getMonth() + 1; //January is 0!

  var yyyy = date.getFullYear();
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }
  date = dd + "-" + mm + "-" + yyyy;
  return date.toString();
}
export default DetailsScreen;