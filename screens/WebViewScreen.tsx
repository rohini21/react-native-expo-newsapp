import * as React from 'react';
import { StyleSheet,Dimensions } from 'react-native';

import { Text, View,  } from '../components/Themed';
import {WebView,} from 'react-native-webview'  
const initialHTMLContent = `
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src='https://unpkg.com/rahulrsingh09-stenciltest2@0.0.3/dist/test/test.js'></script>
</head>
<body>
<h1>Hello</h1>
<my-component source-url="/api/video/v1/us-west-2.893648527354.channel.DmumNckWFTqz.m3u8"></my-component>
</body>
</html>`;
export default class WebViewScreen extends React.Component {
  urlToOpen = 'https://www.newslaundry.com/';
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.urlToOpen = props.route.params.data;
  }
  render() {

    return (
      <View
      style={{
        flex: 1,
      }}>
    <WebView
        source={{
          uri: this.urlToOpen
        }}
        style={{ marginTop: 20 }}
      />
    </View>
      // <Text>Hello</Text>
  //       <WebView
  //  style={styles.webview}
  //  source={{uri: 'https://www.slack.com'}}
  //  javaScriptEnabled={true}
  //  domStorageEnabled={true}
  //  startInLoadingState={false}
  //  scalesPageToFit={true} />
    );
  }
}

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const styles3 = StyleSheet.create({
  webview: {
    flex: 1,
    backgroundColor: 'yellow',
    width: deviceWidth,
    height: deviceHeight
  }
});
const styles2 = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  video: {
    marginTop: 20,
    maxHeight: 200,
    width: 320,
    flex: 1
  }
});