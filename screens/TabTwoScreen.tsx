import * as React from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import axios from 'axios';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "First Item",
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Second Item",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72",
    title: "Third Item",
  }
];

export default class TabTwoScreen extends React.Component {
  newsData:any = []
  // [ {
  //   "source": {
  //     "id": null,
  //     "name": "News-Medical.Net"
  //   },
  //   "author": null,
  //   "title": "Difference in antibody evolution could predict COVID-19 outcomes - News-Medical.Net",
  //   "description": "For COVID-19, the difference between surviving and not surviving severe disease may be due to the quality, not the quantity, of the patients' antibody development and response, suggests a new Cell paper published by Galit Alter, PhD, a member of the Ragon Ins…",
  //   "url": "https://www.news-medical.net/news/20201116/Difference-in-antibody-evolution-could-predict-COVID-19-outcomes.aspx",
  //   "urlToImage": "https://www.news-medical.net/image.axd?picture=2014%2f7%2fAntibody-620x480.jpg",
  //   "publishedAt": "2020-11-16T06:06:00Z",
  //   "content": "Reviewed by Emily Henderson, B.Sc.Nov 16 2020\r\nFor COVID-19, the difference between surviving and not surviving severe disease may be due to the quality, not the quantity, of the patients' antibody d… [+2863 chars]"
  // },
  // {
  //   "source": {
  //     "id": null,
  //     "name": "Hindustan Times"
  //   },
  //   "author": "hindustantimes.com",
  //   "title": "‘He has a big say in Indian cricket’: Greg Chappell’s high-praise for India star - Hindustan Times",
  //   "description": "“I think he’s certainly one of the best players and one of the most influential players in world cricket at the moment,” Chappel told ‘Sydney Morning Herald’ according to PTI.",
  //   "url": "https://www.hindustantimes.com/cricket/he-has-a-big-say-in-indian-cricket-greg-chappell-s-high-praise-for-india-star/story-PFFzCT8Z8FMAwHnlaliRoJ.html",
  //   "urlToImage": "https://www.hindustantimes.com/rf/image_size_960x540/HT/p2/2020/11/16/Pictures/england-nets-press-conference_7965bc32-27cd-11eb-bd4c-84783e11e201.jpg",
  //   "publishedAt": "2020-11-16T05:43:09Z",
  //   "content": "Former Australia batsman Greg Chappell, who also served as the head coach of India from 2005-07, said current Indian skipper Virat Kohli is ‘one of the most influential players in world cricket’ who … [+1535 chars]"
  // }]
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }
  componentDidMount(){
    fetch("https://newsapi.org/v2/top-headlines?country=in&category=politics&apiKey=b2120b045e1c458089cce51320f6a107")
    .then(res => res.json())
    .then(
      (result) => {
        this.newsData = result.articles;
        // console.log('en '+ data)
        this.setState({
          isLoaded: true,
          items: result.items
        });
       
      },
      (error) => {
        
        this.setState({
          isLoaded: true,
          error: false
        });
      }
    )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <Text style={{color: '#fff'}}>Error: {error.message}</Text>;
    } else if (!isLoaded) {
      return <Text style={{color: '#fff'}}>Loading...</Text>;
    } else {
      return (
          
        <FlatList style={{backgroundColor: "#fff", padding: 15,paddingTop: 30}}
        data={this.newsData}
        renderItem={({ item, index }) => {
            return  <TouchableOpacity style={[styles.shadow, {backgroundColor: "#fff"}]}
            onPress={
              () =>this.props.navigation.navigate('DetailsScreen', {data: item})
            }
            style={{backgroundColor:'#fff',color:'black',padding:10, marginTop: 8}}>
             <Text style={{color: '#000'}}>{item.title} </Text>
             
              </TouchableOpacity>
          }}
          
          keyExtractor={item => (item.publishedAt+item.source.id)}
   
      />
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  shadow: {
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: '#fff',
    elevation: 2, // Android
    height: 50,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  }
});
