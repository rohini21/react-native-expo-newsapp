import * as React from 'react';
import { FlatList, StyleSheet, TouchableOpacity, Image } from 'react-native';
import axios from 'axios';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "First Item",
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Second Item",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72",
    title: "Third Item",
  }
];

export default class TrendingScreen extends React.Component {
  newsData: any = [];
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }
  componentDidMount() {
    fetch("https://newsapi.org/v2/top-headlines?country=in&apiKey=b2120b045e1c458089cce51320f6a107")
      .then(res => res.json())
      .then(
        (result) => {
          this.newsData = result.articles;

          // console.log('en '+ data)
          this.setState({
            isLoaded: true,
            items: result.items
          });

        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <Text>Error: {error.message}</Text>;
    } else if (!isLoaded) {
      return <Text>Loading...</Text>;
    } else {
      return (
        <View style={styles.container}>
          <Text style={styles.trending}>Trending</Text>
          <FlatList style={styles.flatlist}
            data={this.newsData}
            horizontal={true}
            renderItem={({ item, index }) => {
              return <TouchableOpacity
                onPress={
                  () => this.props.navigation.navigate('DetailsScreen', { data: item })
                }
                style={{ backgroundColor: 'white', color: 'black', marginTop: 8 }}>
                <View style={styles.tcontainer}>
                  <Text ellipsizeMode='tail' numberOfLines={2} style={styles.heading}>{item.title} </Text>
                  <Image style={[styles.image, { opacity: (item.urlToImage !== null) ? 1 : 0.5 }]}
                    source={{ uri: (item.urlToImage !== null) ? item.urlToImage : 'https://yt3.ggpht.com/ytc/AAUvwnhAEgmu3mhGPYeOluQJtVnVwiZ1wQPFmJTcN-XY1g=s900-c-k-c0x00ffffff-no-rj' }}
                  />

                </View>
              </TouchableOpacity>
            }}

            keyExtractor={item => (item.publishedAt + item.source.id)}

          />
          <Text style={{ fontSize: 16, fontWeight: '500', marginBottom: 12, color: '#000' }}>Today's Read</Text>
          <View style={{ display: "flex", flexDirection: "row", backgroundColor: '#fff' }}>
            <View style={{ marginRight: 30 }}>
              <TouchableOpacity style={{backgroundColor: '#fff'}}
               onPress={
                () => this.props.navigation.navigate('Tech')
              }>
                <Image style={{ height: 115, width: 130, borderRadius: 25 }}
                  source={{ uri: 'https://s27389.pcdn.co/wp-content/uploads/2019/10/retail-innovation-changing-tech-consumer-employee-demands-1024x440.jpeg' }}
                />
                <Text style={[styles.trending, {marginLeft: 14, marginTop: 8}]}>Tech</Text>
              </TouchableOpacity>
            </View>

            <View>
              <TouchableOpacity style={{backgroundColor: '#fff'}}
               onPress={
                () => this.props.navigation.navigate('Politics')
              }>
                <Image style={{ height: 115, width: 130, borderRadius: 25 }}
                  source={{ uri: 'https://image.cnbcfm.com/api/v1/image/106240744-1573590258713politicaldebate.jpg?v=1573590321&w=1400&h=950' }}
                />
                   <Text style={[styles.trending, {marginLeft: 14, marginTop: 8}]}>Politics</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  flatlist: {
    height: 340,
    flexGrow: 0,
    backgroundColor: "#fff"
  },
  trending: {
    fontSize: 10,
    fontWeight: '500',
    color: '#d51014',
    textTransform: "uppercase",
    letterSpacing: 1.5
  },
  heading: {
    fontSize: 14,
    fontWeight: '500',
    marginBottom: 12,
    color: '#3c3c3c',
    paddingRight: 50,
    height: 58

  },
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 30,
    backgroundColor: '#fff'
  },
  image: {
    width: '100%',
    height: 200,
    marginBottom: 8,
    borderRadius: 32,
    borderColor: "#f2f2f2",
    borderWidth: 1,
    borderStyle: "solid",
  },
  tcontainer: {
    width: 300,
    marginRight: 10,
    backgroundColor: "#fff"
  }
});
