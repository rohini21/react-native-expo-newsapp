import * as React from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { Text, View } from '../components/Themed';
const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "First Item",
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Second Item",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72",
    title: "Third Item",
  }
];

export default class TabOneScreen extends React.Component {
  newsData:any = [];
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }
  componentDidMount(){
    fetch("https://newsapi.org/v2/top-headlines?country=in&category=technology&apiKey=b2120b045e1c458089cce51320f6a107")
    .then(res => res.json())
    .then(
      (result) => {
        this.newsData = result.articles;
        
        // console.log('en '+ data)
        this.setState({
          isLoaded: true,
          items: result.items
        });
       
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <Text>Error: {error.message}</Text>;
    } else if (!isLoaded) {
      return <Text>Loading...</Text>;
    } else {
      return (
        <FlatList style={{backgroundColor: "#fff", padding: 15,paddingTop: 30}}
        data={this.newsData}
        renderItem={({ item, index }) => {
            return  <TouchableOpacity style={[styles.shadow, {backgroundColor: "#fff"}]}
            onPress={
              () =>this.props.navigation.navigate('DetailsScreen', {data: item})
            }
            style={{backgroundColor:'#fff',color:'black',padding:10, marginTop: 8}}>
             <Text style={{color: '#000'}}>{item.title} </Text>
             
              </TouchableOpacity>
          }}
          
          keyExtractor={item => (item.publishedAt+item.source.id)}
   
      />
   
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  video: {
    marginTop: 20,
    maxHeight: 200,
    width: 320,
    flex: 1
  }
});
