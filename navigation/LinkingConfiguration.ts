import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Trending: {
            screens: {
              TrendingScreen: 'Trending',
            },
          },
          Tech: {
            screens: {
              TabOneScreen: 'tech',
            },
          },
          Politics: {
            screens: {
              TabTwoScreen: 'politics',
            },
          }
        },
      },
      NotFound: '*',
    },
  },
};
