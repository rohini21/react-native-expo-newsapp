import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import TabOneScreen from '../screens/TabOneScreen';
import TabTwoScreen from '../screens/TabTwoScreen';
import DetailsScreen from '../screens/DetailsScreen';
import WebViewScreen from '../screens/WebViewScreen';
import TrendingScreen from  '../screens/TrendingScreen';
import { BottomTabParamList, TabOneParamList, TabTwoParamList } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();
const data = ['a', 'b']
export default function BottomTabNavigator() {

  return (
    <BottomTab.Navigator
      initialRouteName="Trending"
      tabBarOptions={{ activeTintColor: '#d51014', style:{
        backgroundColor: '#fff',
        paddingTop: 5
      },

      labelStyle: {
        fontSize: 13,
        margin: 0,
        paddingTop: 5,
        paddingBottom: 4,
      },
       }}>
        <BottomTab.Screen
        name="Trending"
        component={TrendingNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-trending-up" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Politics"
        component={TabTwoNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-school" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Tech"
        component={TabOneNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="logo-apple" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
  // console.log('api val', fetchNewsData())
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name="TabOneScreen"
        component={TabOneScreen}
        options={{ title: 'Tech Headlines'}}
        
      />
       <TabOneStack.Screen
        name="DetailsScreen"
        component={DetailsScreen}
        options={{headerShown: false}}
      />
       <TabOneStack.Screen
        name="WebViewScreen"
        component={WebViewScreen}
        options={{ title: 'Full Article'}}
      />
    </TabOneStack.Navigator>
  );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="TabTwoScreen"
        component={TabTwoScreen}
        options={{ title: 'Politics Headlines'}}
      />
          <TabOneStack.Screen
        name="DetailsScreen"
        component={DetailsScreen}
        options={{headerShown: false}}
      />
       <TabOneStack.Screen
        name="WebViewScreen"
        component={WebViewScreen}
        options={{ title: 'Full Article'}}
      />
    </TabTwoStack.Navigator>
  );
}

function TrendingNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="Trending"
        component={TrendingScreen}
        options={{headerShown: false}}
      />
          <TabOneStack.Screen
        name="DetailsScreen"
        component={DetailsScreen}
        options={{headerShown: false}}
      />
       <TabOneStack.Screen
        name="WebViewScreen"
        component={WebViewScreen}
        options={{ title: 'Full Article'}}
      />
    </TabTwoStack.Navigator>
  );
}

